﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assoziation
{
    class Auto
    {
        private string name


        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        private double km;

        public double Km
        {
            get { return km; }
            set { km = value; }
        }

        // 0 vl, 1 vr, 2 hl, 3 hr
        private Reifen[] reifenSatz = new Reifen[4];

        public void fahren(double gefahren)
        {
            km = km + gefahren;

        }

        /// <summary>
        /// Montiere einen Reifen (r) aus bestimmer Position (pos)
        /// </summary>
        /// <param name="pos"> Integer mit Reifenposition (0 vl, 1 vr, 2 hl, 3 hr)</param>
        /// <param name="r"></param>
        public void reifenMontieren(int pos,Reifen r)
        {
            reifenSatz[pos] = r;
        }
    }
}
