﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assoziation
{
    class Program
    {
        static void Main(string[] args)
        {
            Auto meinAuto = new Auto();
            Reifen vl = new Reifen();
            Reifen vr = new Reifen();
            Reifen hl = new Reifen();
            Reifen hr = new Reifen();

            #region Reifendefinition
            vl.Mm = 10; vl.Baujahr = "2005"; vl.Winter = false;
            vr.Mm = 11; vr.Baujahr = "2017"; vr.Winter = false;
            hl.Mm = 12; hl.Baujahr = "2009"; hl.Winter = false;
            hr.Mm = 13; hr.Baujahr = "2011"; hr.Winter = false;
            #endregion

            meinAuto.Name = "Civic";
            meinAuto.Km = 145387;

            #region Reifen montieren
            meinAuto.reifenMontieren(0, vl);
            meinAuto.reifenMontieren(1, vr);
            meinAuto.reifenMontieren(2, hl);
            meinAuto.reifenMontieren(3, hr);
            #endregion

            meinAuto.fahren(38.4);

            Console.WriteLine("Gefahren: " + meinAuto.Km);

            #region Ausgabe Reifen
            Console.WriteLine("Reifen Vorne Links:");
            Console.WriteLine("   Baujahr: " + vl.Baujahr);
            Console.WriteLine("   Profiltiefe: " + vl.Mm);

            Console.WriteLine("Reifen Vorne Rechts:");
            Console.WriteLine("   Baujahr: " + vr.Baujahr);
            Console.WriteLine("   Profiltiefe: " + vr.Mm);

            Console.WriteLine("Reifen Hinten Links:");
            Console.WriteLine("   Baujahr: " + hl.Baujahr);
            Console.WriteLine("   Profiltiefe: " + hl.Mm);

            Console.WriteLine("Reifen Hinten Rechts:");
            Console.WriteLine("   Baujahr: " + hr.Baujahr);
            Console.WriteLine("   Profiltiefe: " + hr.Mm);
            #endregion

            Console.ReadLine();




        }
    }
}
